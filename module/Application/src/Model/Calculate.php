<?php

namespace Application\Model;

 class Calculate
 {

    public function question_1($n,$s,$loop)
    {
        $numbers[] = $s;
        $numbers[] = $n;

        $diff = $n - $s;

        for ($i=0; $i <$loop ; $i++) { 
            $diff = $diff + 2;
            $n = $n + $diff;
            $numbers[] = $n;
        }

        return $numbers;
    }

    public function answer_1($numbers){

        $text = '';

        foreach ($numbers as $number) {
            $text .= ', '.$number;
        }

        return substr($text,2);
    }

    public function answer_1_var($number){

        return 'X = '.$number;

    }

    public function question_2($n,$nn,$ans){

        return ($ans - $n) - $nn;

    }

    public function answer_2($q,$y){

        $answer = str_replace('  - Please create new function for finding Y value','',$q);
        $answer = str_replace('Y',$y,$answer);
        return $answer;
    }

    public function answer_2_var($number){

        return 'Y = '.$number;

    }

    public function question_3($x,$start,$loop){

        $text = $x;

        for ($i=$start; $i <= $loop ; $i++) { 
            $text = $i.$text;
            $x = $text;
         }

        return $x;

    }

    public function answer_3($q,$x){

        $answer = str_replace(' - Please create new function for finding X value','',$q);
        $answer = str_replace('X',$x,$answer);
        return $answer;
    }

    public function answer_3_var($number){

        return 'X = '.$number;

    }
}