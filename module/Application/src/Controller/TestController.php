<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Model\Calculate;

class TestController extends AbstractActionController
{

    protected $calculate;

    public function __construct()
    {
        $this->calculate = new Calculate(); 

        $seconds_to_cache = 3600;
        $ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
        header("Expires: $ts");
        header("Pragma: cache");
        header("Cache-Control: max-age=$seconds_to_cache");
    }
    
    public function indexAction()
    {
        redirect('/');
        exit;
    }

    public function firstAction()
    {
        $question = '3, 5, 9, 15, X  - Please create new function for finding X value';

        $s = 3;
        $n = 5;
        //Setting Varible

        $numbers = $this->calculate->question_1($n,$s,3); 
        //Use Model to calculate

        $answer_var = $this->calculate->answer_1_var(end($numbers)); 
        $answer = $this->calculate->answer_1($numbers); 

        return new ViewModel(array(
            'question' => $question,
            'answer' => $answer,
            'answer_var' => $answer_var
        ));
    }

    public function secondAction()
    {
        $question = '(Y + 24)+(10 × 2) = 99  - Please create new function for finding Y value';
        $ans = 99;
        $n = 10*2;
        $nn = 24;

        $y = $this->calculate->question_2($n,$nn,$ans); 
        //Use Model to calculate

        $answer_var = $this->calculate->answer_2_var($y);
        $answer = $this->calculate->answer_2($question,$y); 

        return new ViewModel(array(
            'question' => $question,
            'answer' => $answer,
            'answer_var' => $answer_var
        ));
    }

    public function thirdAction()
    {
        $question = 'If 1 = 5 , 2 = 25 , 3 = 325 , 4 = 4325 Then 5 = X - Please create new function for finding X value';

        $x = 5;
        $x = $this->calculate->question_3($x,2,5); 
        //Use Model to calculate

        $answer_var = $this->calculate->answer_3_var($x);
        $answer = $this->calculate->answer_3($question,$x);

        return new ViewModel(array(
            'question' => $question,
            'answer' => $answer,
            'answer_var' => $answer_var
        ));
    }
}
